package controller;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import cotizacion.Cotizacion;
import cotizacion.DbCotizacion;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import views.dlgManejo;

public class Controlador implements ActionListener {

    DbCotizacion db;
    Cotizacion cot;
    dlgManejo vista;

    private boolean isActualizar = false;

    
    public Controlador(DbCotizacion db, dlgManejo vista) {
        this.db = db;
        this.vista = vista;
        cot = new Cotizacion();
        
        vista.btnBuscar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnBorrar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.btnNuevo) {
            this.habilitar();
        }
        else if (e.getSource() == vista.btnGuardar) {
            try {
                cot.setNumCotizacion(vista.txtNumCotizacion.getText());
                cot.setDescripcionAutomovil(vista.txtDescripcion.getText());
                cot.setPorcentajePago(Integer.parseInt(vista.spnPorcentaje.getValue().toString()));
                cot.setPrecioAutomovil(Integer.parseInt(vista.txtPrecio.getText()));

                if (vista.rdb12.isSelected()) {
                    cot.setPlazo(12);
                }
                if (vista.rdb24.isSelected()) {
                    cot.setPlazo(24);
                }
                if (vista.rdb36.isSelected()) {
                    cot.setPlazo(36);
                }
                if (vista.rdb48.isSelected()) {
                    cot.setPlazo(48);
                }
                if (vista.rdb60.isSelected()) {
                    cot.setPlazo(60);
                }
                if(isActualizar==true){
                    db.actualizar(cot);
                    JOptionPane.showMessageDialog(vista, "Se actualizo con exito");
                    limpiar(); 
                }
                else if(db.buscar(cot.getNumCotizacion()).equals(-1)){
                    db.insertar(cot);
                    JOptionPane.showMessageDialog(vista, "Se agrego con exito");
                    limpiar();
                } else { 
                    JOptionPane.showMessageDialog(vista, "El codigo ya existe");
                    limpiar(); 
                } 
            }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "No se pudo guardar, surgio el siguiente error: "+ex.getMessage());
            }
            catch(Exception ex2){
                 JOptionPane.showMessageDialog(vista, "No se pudo guardar, surgio el siguiente error: "+ex2.getMessage());
            } 
            tablaM();
            isActualizar=false;
        }else if(e.getSource() == vista.btnBuscar){
            try{
                cot = (Cotizacion) db.buscar(vista.txtNumCotizacion.getText());
                cot.setNumCotizacion(vista.txtNumCotizacion.getText());
                cot=cot;
                if(!db.buscar(cot.getNumCotizacion()).equals(-1)){
                    limpiar();
                    vista.txtNumCotizacion.setText(cot.getNumCotizacion());
                    vista.txtDescripcion.setText(cot.getDescripcionAutomivil());
                    vista.txtPrecio.setText(String.valueOf(cot.getPrecioAutomovil()));
                    
                    switch (cot.getPlazo()) {
                        case 12: vista.rdb12.setSelected(true); break;
                        case 24: vista.rdb24.setSelected(true); break;
                        case 36: vista.rdb36.setSelected(true); break;
                        case 48: vista.rdb48.setSelected(true); break;
                        case 60: vista.rdb60.setSelected(true); break;
                    }
                    
                    vista.spnPorcentaje.setValue(cot.getPorcentajePago());
                    vista.txtPagoInicial.setText(String.valueOf(cot.calcularPagoInicial()));
                    vista.txtTotalFin.setText(String.valueOf(cot.calcularTotalAfinanciar()));
                    vista.txtPagoMensual.setText(String.valueOf(cot.calcularPagoMensual()));
                    this.isActualizar=true;
                }
                else{                    
                    JOptionPane.showMessageDialog(vista, "No se encontro ");
                }
            }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(vista, "No se pudo encontrar, surgio el siguiente error: "+ex.getMessage());
            }
            catch(Exception ex2){
                 JOptionPane.showMessageDialog(vista, "No se pudo encontrar, surgio el siguiente error: "+ex2.getMessage());
            } 
            
        }else if(e.getSource() == vista.btnBorrar){
            int opcion = 0;
            cot.setNumCotizacion(vista.txtNumCotizacion.getText());
            
            opcion = JOptionPane.showConfirmDialog(vista, "¿Desea borrar el registro?", "Cotizacion",JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            
            if(opcion == JOptionPane.YES_OPTION){
                try{
                    db.deshabilitar(cot);
                    JOptionPane.showMessageDialog(vista, "Se borro el registro con exito");
                    this.limpiar();
                    this.deshabilitar();
                    
                }catch(NumberFormatException ex){
                    JOptionPane.showMessageDialog(vista, "No se pudo guardar, surgio el siguiente error: "+ex.getMessage());
                }
                catch(Exception ex2){
                     JOptionPane.showMessageDialog(vista, "No se pudo guardar, surgio el siguiente error: "+ex2.getMessage());
                }
            }
            tablaM();
            
        }else if(e.getSource() == vista.btnLimpiar){
            this.limpiar();
        }else if(e.getSource() == vista.btnCancelar){
            this.limpiar();
            this.deshabilitar();
        }else if(e.getSource() == vista.btnCerrar){
           int option=JOptionPane.showConfirmDialog(vista,"¿Deseas salir?",
            "Decide", JOptionPane.YES_NO_OPTION);
             if(option==JOptionPane.YES_NO_OPTION){
                 vista.dispose();
                 System.exit(0);
             }
        }
    }

    public void habilitar() {
        vista.txtNumCotizacion.setEnabled(true);
        vista.txtDescripcion.setEnabled(true);
        vista.txtPrecio.setEnabled(true);
        vista.spnPorcentaje.setEnabled(true);
        vista.pnlPlazos.setEnabled(true);
        vista.btnGuardar.setEnabled(true);
        vista.btnBuscar.setEnabled(true);
        vista.btnBorrar.setEnabled(true);
    }

    public void deshabilitar() {
        vista.txtNumCotizacion.setEnabled(false);
        vista.txtDescripcion.setEnabled(false);
        vista.txtPrecio.setEnabled(false);
        vista.spnPorcentaje.setEnabled(false);
        vista.pnlPlazos.setEnabled(false);
        vista.btnGuardar.setEnabled(false);
    }

    public void limpiar() {
        vista.txtNumCotizacion.setText("");
        vista.txtDescripcion.setText("");
        vista.txtPrecio.setText("");
        vista.spnPorcentaje.setValue(20);
        vista.rdb12.setSelected(true);
        vista.txtPagoInicial.setText("");
        vista.txtPagoMensual.setText("");
        vista.txtTotalFin.setText("");
    }
    private void tablaM(){
        DefaultTableModel modelo = new DefaultTableModel();
        ArrayList<Cotizacion> lista = new ArrayList<>();
        try {
            lista = db.listar();
        } catch (Exception ex) {
        }
        modelo.addColumn("idCotizacion");
        modelo.addColumn("numCotizacion");
        modelo.addColumn("descripcion");
        modelo.addColumn("precio");
        modelo.addColumn("porcentaje");
        modelo.addColumn("plazos");
        for (Cotizacion producto : lista) {
            modelo.addRow(new Object[]{producto.getIdCotizacion(), producto.getNumCotizacion(), producto.getDescripcionAutomivil(), producto.getPrecioAutomovil(), producto.getPorcentajePago(), producto.getPlazo()});
        }
        vista.jtbListaAlumnos.setModel(modelo);
    }
    private void iniciarVista(){
        
        //vista.jTableDes.setModel(db.listarDes());
        tablaM();
        vista.setTitle(":: COTIZACION ::");
        vista.setSize(700, 700);
        vista.setVisible(true);
    }

    public static void main(String[] args) {
        DbCotizacion db = new DbCotizacion();
        dlgManejo vista = new dlgManejo(); 
        Controlador con = new Controlador(db, vista);

        con.iniciarVista();
        
        /*cot.setNumCotizacion("123");
        cot.setDescripcionAutomovil("Toyota Cambry");
        cot.setPrecioAutomovil(350000);
        cot.setPlazo(60);
        cot.imprimirCotizacion();

        db.insertar(cot);*/
    }
}
