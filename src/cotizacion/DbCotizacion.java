/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package cotizacion;
import java.sql.*;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class DbCotizacion extends DbManejador implements DbPersistencia {

    @Override
    public boolean insertar(Object objecto) throws Exception {
        Cotizacion cot = new Cotizacion();
        cot=(Cotizacion) objecto;
        String consulta="insert into cotizaciones(numCotizacion,descripcion,precio,porcentaje, plazos) values(?,?,?,?,?)";
        
        if(this.conectar()){
            try{
                System.err.println("Se conecto");
                this.sqlConsulta=conexion.prepareStatement(consulta);
                //INGRESAR VALORES A LA CONSULTA
                this.sqlConsulta.setString(1,cot.getNumCotizacion());
                this.sqlConsulta.setString(2,cot.getDescripcionAutomivil());
                this.sqlConsulta.setInt(3,cot.getPrecioAutomovil());
                this.sqlConsulta.setInt(4,cot.getPorcentajePago());
                this.sqlConsulta.setInt(5, cot.getPlazo());
               // this.sqlConsulta.setInt(6,1);

                this.sqlConsulta.executeUpdate();
                System.out.println("Se agrego exitosamente");
                return true;
            } catch (SQLException e) {
                System.out.println("Surgio un error al insertar "+e.getMessage());
            }            
        }
        else{
            System.out.println("No fue posible concectarse");
        }
        this.desconectar();
        return false;
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void actualizar(Object objecto) throws Exception {
        Cotizacion cot = new Cotizacion();
        cot=(Cotizacion) objecto;
        String consulta = "update cotizaciones set descripcion = ?, precio= ?,porcentaje=?, plazos=? where numCotizacion = ?";
        
        if(this.conectar()){
            try {
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                // ASIGNAR VALORES DEL CAMPO 
                this.sqlConsulta.setString(5,cot.getNumCotizacion());
                this.sqlConsulta.setString(1,cot.getDescripcionAutomivil());
                this.sqlConsulta.setInt(2,cot.getPrecioAutomovil());
                this.sqlConsulta.setInt(3,cot.getPorcentajePago()); 
                this.sqlConsulta.setInt(4, cot.getPlazo());

                this.sqlConsulta.executeUpdate();
                
                System.out.println("Se actualizo");
                
            } catch (Exception e) {
                System.out.println("Surgio un error al actualizar "+e.getMessage());
            }
            this.desconectar();
        }
        
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean habilitar(Object objecto) throws Exception {
        Cotizacion cot = new Cotizacion();
        
        cot=(Cotizacion) objecto;
        String consulta = "update cotizacion set status=1 where numCotizacion = ?";
        
        if(this.conectar()){
            try {
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                // ASIGNAR VALORES DEL CAMPO 
                this.sqlConsulta.setString(1,cot.getNumCotizacion()); 

                this.sqlConsulta.executeUpdate();
                
                System.out.println("Se Habilito");
                this.desconectar();
                return true;
                
            } catch (Exception e) {
                System.out.println("Surgio un error al Habilitar "+e.getMessage());
            } 
            this.desconectar();
        }
        return false;
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean deshabilitar(Object objecto) throws Exception {
        Cotizacion cot = new Cotizacion();
        
        cot=(Cotizacion) objecto;
        String consulta = "delete from cotizaciones where numCotizacion = ?";
        
        if(this.conectar()){
            try {
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                // ASIGNAR VALORES DEL CAMPO 
                this.sqlConsulta.setString(1,cot.getNumCotizacion()); 

                this.sqlConsulta.executeUpdate();
                this.desconectar();
                return true;
                
            } catch (Exception e) {
                System.out.println("Surgio un error al eliminar "+e.getMessage());
            }
            this.desconectar();
        }
        return false;
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public boolean isExiste(String numCotizacion) throws Exception {
       //BUSCAR SI YA EXISTE UNA COTIZACION- REGRESA FALSE SI EXISTE.
       Cotizacion cot = new Cotizacion();
       if(this.conectar()){
            String consulta ="Select * from cotizaciones where numCotizacion=?";
            this.sqlConsulta=this.conexion.prepareStatement(consulta);
            //ASIGNAR VALORES
            this.sqlConsulta.setString(1, numCotizacion);
            //Hacer la consulta
            this.registros=this.sqlConsulta.executeQuery();
            //Sacar los registros
            if(this.registros.next()){
                this.desconectar();
                return false;
            }
       }this.desconectar();
       return true;        
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ArrayList listar() throws Exception {
        ArrayList<Cotizacion> lista = new ArrayList<Cotizacion>();
        Cotizacion cot;

        if(this.conectar()){
            String consulta = "SELECT * from cotizaciones ORDER BY numCotizacion";
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.registros = this.sqlConsulta.executeQuery();

            while(this.registros.next()){
                cot = new Cotizacion();

                cot.setIdCotizacion(this.registros.getInt("idCotizacion"));
                cot.setNumCotizacion(this.registros.getString("numCotizacion"));
                cot.setDescripcionAutomovil(this.registros.getString("descripcion"));
                cot.setPrecioAutomovil(this.registros.getInt("precio"));
                cot.setPorcentajePago(this.registros.getInt("porcentaje"));
                cot.setPlazo(this.registros.getInt("plazos"));
                lista.add(cot);
            }
        }
        this.desconectar();
        return lista;
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ArrayList listar(String criterio) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public DefaultTableModel listarD() throws Exception {
        Cotizacion cot;
        if(this.conectar()){ 
            String sql = "select * from sistemas.cotizacion where status = 0 order by codigo";
            DefaultTableModel Tabla = new DefaultTableModel();

            JTable VistaTabla = new JTable(Tabla);
            PreparedStatement pst = conexion.prepareStatement(sql);
            ResultSet rs = pst.executeQuery();
            ResultSetMetaData Datos = rs.getMetaData();

            //Agregar Columnas
            for (int column = 1; column < Datos.getColumnCount(); column++) {
                Tabla.addColumn(Datos.getColumnLabel(column));
            }
            //Agregar Tablas
            while (rs.next()) {
                    Object[] row = new Object[Datos.getColumnCount()];
                    for (int column = 1; column <= Datos.getColumnCount(); column++) {
                        row[column - 1] = rs.getString(column);
                    }
                    Tabla.addRow(row);
                }
            return Tabla;       
        }         
        return null;
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public ArrayList listarD(String criterio) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Object buscar(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Object buscar(String numCotizacion) throws Exception { 
       Cotizacion cot = new Cotizacion();
       if(this.conectar()){
            String consulta ="Select * from cotizaciones where numCotizacion=?";
            this.sqlConsulta=this.conexion.prepareStatement(consulta);
            //ASIGNAR VALORES
            this.sqlConsulta.setString(1, numCotizacion);
            //Hacer la consulta
            this.registros=this.sqlConsulta.executeQuery();
            //Sacar los registros
            if(this.registros.next()){
                cot.setIdCotizacion(this.registros.getInt("idCotizacion"));
                cot.setNumCotizacion(this.registros.getString("numCotizacion"));
                cot.setDescripcionAutomovil(this.registros.getString("descripcion"));
                cot.setPrecioAutomovil(this.registros.getInt("precio"));
                cot.setPorcentajePago(this.registros.getInt("porcentaje"));
                cot.setPlazo(this.registros.getInt("plazos"));
              //  cot.setStatus(this.registros.getInt("status"));
            }
            else {
                this.desconectar();
                return -1;
            
            }
       }this.desconectar();
       this.desconectar();
       return cot;
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Object buscarD(int id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Object buscarD(String numCotizacion) throws Exception {
             /*idCotizacion - numCotizacion - descripcionAutomovil - 
            precioAutomovil - porcentajePago - plazo*/
       Cotizacion cot = new Cotizacion();
       if(this.conectar()){
            String consulta ="Select * from productos where numCotizacion=? and status=0";
            this.sqlConsulta=this.conexion.prepareStatement(consulta);
            //ASIGNAR VALORES
            this.sqlConsulta.setString(1, numCotizacion);
            //Hacer la consulta
            this.registros=this.sqlConsulta.executeQuery();
            //Sacar los registros
            if(this.registros.next()){
                cot.setIdCotizacion(this.registros.getInt("idCotizacion"));
                cot.setNumCotizacion(this.registros.getString("numCotizacion"));
                cot.setDescripcionAutomovil(this.registros.getString("descripcionAutomovil"));
                cot.setPrecioAutomovil(this.registros.getInt("precioAutomovil"));
                cot.setPorcentajePago(this.registros.getInt("porcentajePago"));
              //  cot.setStatus(this.registros.getInt("status"));
            }
            else {
                return -1;
            
            }
       }
       this.desconectar();
       return cot;
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
    
}
