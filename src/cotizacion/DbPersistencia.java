/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package cotizacion;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

public interface DbPersistencia {
    public boolean insertar(Object objecto) throws Exception;
    public void actualizar(Object objecto) throws Exception;
    public boolean habilitar(Object objecto) throws Exception;
    public boolean deshabilitar(Object objecto) throws Exception;
    
    public boolean isExiste(String codigo) throws Exception;
    public ArrayList listar() throws Exception;
    public ArrayList listar(String criterio) throws Exception ;
    public DefaultTableModel listarD() throws Exception;
    public ArrayList listarD(String criterio) throws Exception ;
    
    public Object buscar(int id) throws Exception;
    public Object buscar(String codigo) throws Exception;
    public Object buscarD(int id) throws Exception;
    public Object buscarD(String codigo) throws Exception;
}
